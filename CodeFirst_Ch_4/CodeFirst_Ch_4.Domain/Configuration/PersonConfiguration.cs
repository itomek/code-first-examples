﻿using CodeFirst_Ch_4.Model;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations;

namespace CodeFirst_Ch_4.Domain.Configuration
{
	public class PersonConfiguration : EntityTypeConfiguration<Person>
	{
		public PersonConfiguration()
		{
			this.HasKey(k => k.SocialSecurityNumber);

			// note that this is inserted as NONE so that the database doesn't auto incrememnt the key. This is
			// so because we want to use Social Security number as the key
			this.Property(p => p.SocialSecurityNumber).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(p => p.SocialSecurityNumber).IsConcurrencyToken();

			// setting field for concurrancy
			this.Property(p => p.RowVersion).IsRowVersion();
		}
	}
}
