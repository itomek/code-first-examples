﻿using CodeFirst_Ch_3.Model;
using System.Data.Entity.ModelConfiguration;

namespace CodeFirst_Ch_3.Domain.Configuration
{
	/// <summary>
	/// Note that this is a complex type which contains a complex type we don't actually set any properties here, but just because it inherits from "ComplexTypeConfiguration", Code First knows its a complex type
	/// </summary>
	public class PersonalInfoConfiguration : ComplexTypeConfiguration<PersonalInfo>
	{
		public PersonalInfoConfiguration()
		{

		}
	}
}
