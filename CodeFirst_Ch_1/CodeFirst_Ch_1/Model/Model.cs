﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirst_Ch_1.Model
{
	public class Patient
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public DateTime BirthDate { get; set; }
		public AnimalType AnimalType { get; set; }
		public List<Visit> Visits { get; set; }
	}

	public class Visit
	{
		public int Id { get; set; }
		public DateTime Date { get; set; }
		public string ReasonForVisit { get; set; }
		public string Outcome { get; set; }
		public decimal Weight { get; set; }
		public int PatientId { get; set; }
	}

	public class AnimalType
	{
		public int Id { get; set; }
		public string TypeName { get; set; }
	}
}
