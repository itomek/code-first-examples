﻿
namespace CodeFirst_Ch_4.Model
{
	public class Measurement
	{
		public decimal Reading { get; set; }
		public decimal Units { get; set; }
	}
}
