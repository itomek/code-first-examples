﻿
namespace CodeFirst_Ch_3.Model
{
	/// <summary>
	/// Note that this class uses another complext type. Look at the Person class and see how its instantiated in the constructor
	/// </summary>
	public class PersonalInfo
	{
		public Measurement Weight { get; set; }
		public Measurement Height { get; set; }
		public string DietryRestriction { get; set; }
	}
}