﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeFirst_Ch_3.Domain;
using CodeFirst_Ch_3.Model;
using System.Data.Entity;

namespace CodeFirst_Ch_3.UI
{
	class Program
	{
		static void Main(string[] args)
		{
			// drop and re-create db when model changes
			Database.SetInitializer(new DropCreateDatabaseIfModelChanges<BreakAwayContext_Ch_3>());

			#region Destination

			Console.WriteLine("------------- DESTINATION -------------");
			Console.Write("Country: ");
			string country = Console.In.ReadLine();

			Console.Write("Description: ");
			string description = Console.In.ReadLine();

			Console.Write("Name: ");
			string name = Console.In.ReadLine();

			Console.WriteLine("ID Inserted: {0}.", InsertDestination(country, description, name));

			#endregion

			#region Trip

			Guid resultFromTripInsert;

			Console.WriteLine("------------- TRIP -------------");
			Console.Write("Begin Date: ");
			DateTime beginDate; DateTime.TryParse(Console.In.ReadLine(), out beginDate);

			Console.Write("Description: ");
			DateTime endDate; DateTime.TryParse(Console.In.ReadLine(), out endDate);

			Console.Write("Name: ");
			decimal cost = default(decimal); decimal.TryParse(Console.In.ReadLine(), out cost);

			resultFromTripInsert = InsertTrip(cost, endDate, beginDate);

			Console.WriteLine("Trip {0} inserted.", resultFromTripInsert.ToString());

			#endregion

			#region Trip Update

			Console.WriteLine("------------- TRIP UPDATE -------------");
			Console.Write("New Trip Cost: ");

			string stNewCost = Console.In.ReadLine();

			decimal newCost = default(decimal);
			Decimal.TryParse(String.IsNullOrEmpty(stNewCost) ? "87.32" : stNewCost, out newCost);

			Console.WriteLine("Trip {0} Updated!", UpdateTrip(resultFromTripInsert, newCost));

			#endregion

			#region Person

			Console.WriteLine("------------- PERSON -------------");
			Console.Write("Social Security Number: ");
			long ssn; long.TryParse(Console.In.ReadLine(), out ssn);

			Console.Write("First Name: ");
			string fname = Console.In.ReadLine();

			Console.Write("Last Name: ");
			string lname = Console.In.ReadLine();

			Console.WriteLine("Trip {0} inserted.", InsertPerson(ssn, fname, lname));

			#endregion
		}

		/// <summary>
		/// Save Destination
		/// </summary>
		/// <returns>ID of Destination entered</returns>
		private static int InsertDestination(string country, string description, string name)
		{
			int id = default(int);

			using (var context = new BreakAwayContext_Ch_3())
			{
				id = context.SaveDestination(new Destination()
				{
					Country = String.IsNullOrEmpty(country) ? "Poland" : country,
					Description = String.IsNullOrEmpty(description) ? "A great place to live" : country,
					Name = String.IsNullOrEmpty(name) ? "PL" : name
				});
			}

			return id;
		}

		/// <summary>
		/// Save Trip
		/// </summary>
		/// <returns>ID (as GUID) of Trip saved</returns>
		private static Guid InsertTrip(decimal cost, DateTime end, DateTime start)
		{
			Guid guid;

			using (var context = new BreakAwayContext_Ch_3())
			{
				guid = context.SaveTrip(new Trip()
				{
					StartDate = start == default(DateTime) ? DateTime.Now : start,
					EndDate = end == default(DateTime) ? DateTime.Now : end,
					CostUSD = cost == default(decimal) ? 34.23M : cost
				});
			}

			return guid;
		}

		/// <summary>
		/// Demonstrates the updating of trip and what happens to RowVersion (aka: Timestamp). Check db to see result of update
		/// </summary>
		private static Guid UpdateTrip(Guid id, decimal cost)
		{
			Guid guid;

			using (var context = new BreakAwayContext_Ch_3())
			{
				guid = context.UpdateTrip(id, cost);
			}

			return guid;
		}

		/// <summary>
		/// Save a new Person
		/// </summary>
		/// <returns>ID of person entered</returns>
		private static long InsertPerson(long ssn, string fname, string lname)
		{
			long result = default(int);

			using (var context = new BreakAwayContext_Ch_3())
			{
				long newSSN = ssn;

				if (newSSN == default(long))
					newSSN = new Random().Next(100000000, 999999999);

				result = context.SavePerson(new Person()
				{
					SocialSecurityNumber = newSSN,
					FirstName = String.IsNullOrEmpty(fname) ? "Tomek" : fname,
					LastName = String.IsNullOrEmpty(lname) ? "Janosik" : lname
				});
			}

			return result;
		}
	}
}
