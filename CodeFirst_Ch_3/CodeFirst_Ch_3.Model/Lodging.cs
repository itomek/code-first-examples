﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirst_Ch_3.Model
{
	public class Lodging
	{
		public int LodgingId { get; set; }
		public string Name { get; set; }
		public string Owner { get; set; }
		public bool IsResort { get; set; }
		public decimal MilesFromNearestAirport { get; set; }

		/// <summary>
		/// Note that this name is enough for Code First to determine that this is a Non Nullable Foreign Key that points to the Destination table
		/// </summary>
		public int DestinationId { get; set; }

		public List<InternetSpecial> InternetSpecials { get; set; }
	}
}
