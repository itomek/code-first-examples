﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeFirst_Ch_2.Domain;
using CodeFirst_Ch_2.Model;
using System.Data.Entity;

namespace CodeFirst_Ch_2.UI
{
	class Program
	{
		static void Main(string[] args)
		{
			Database.SetInitializer(new DropCreateDatabaseIfModelChanges<BreakAwayContext_Ch_2>());

			Console.Write("Country: ");
			string country = Console.In.ReadLine();

			Console.Write("Description: ");
			string description = Console.In.ReadLine();

			Console.Write("Name: ");
			string name = Console.In.ReadLine();

			Console.WriteLine("ID Inserted: {0}.", InsertDestination(country, description, name));
		}

		private static int InsertDestination(string country, string description, string name)
		{
			var destination = new Destination()
			{
				Country = country,
				Description = description,
				Name = name
			};

			int id = default(int);

			using (var context = new BreakAwayContext_Ch_2())
			{
				id = context.SaveDestination(destination);
			}

			return id;
		}
	}
}
