﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirst_Ch_4.Model
{
	public class Lodging
	{
		public int LodgingId { get; set; }
		public string Name { get; set; }
		public string Owner { get; set; }
		public bool IsResort { get; set; }
		public decimal MilesFromNearestAirport { get; set; }

		public Person PrimaryContact { get; set; }
		public Person SecondaryContact { get; set; }

		// page 3598
		// Relationships that have Unidrectional Navigation
		//public int LocationId { get; set; }

		/// <summary>
		/// Note that this name is enough for Code First to determine that this is a Non Nullable Foreign Key that points to the Destination table
		/// </summary>
		public int DestinationId { get; set; }

		public Destination Destination { get; set; }

		public List<InternetSpecial> InternetSpecials { get; set; }
	}
}
