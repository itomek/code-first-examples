﻿using System.Data.Entity;
using CodeFirst_Ch_2.Model;
using System.Collections.Generic;

namespace CodeFirst_Ch_2.Domain
{
	public class BreakAwayDataAnnotationsContext_Ch_2 : DbContext
	{
		public DbSet<Destination> Destination { get; set; }
		public DbSet<Lodging> Lodging { get; set; }

		public int SaveDestination(Destination destination)
		{
			Destination newDestination = this.Destination.Add(destination);
			this.SaveChanges();

			return newDestination.DestinationId;
		}
	}
}
