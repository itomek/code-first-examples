﻿using System.Data.Entity.ModelConfiguration;
using CodeFirst_Ch_2.Model;

namespace CodeFirst_Ch_2.Domain.Configuration
{
	public class LodgingConfigurationcs : EntityTypeConfiguration<Lodging>
	{
		public LodgingConfigurationcs()
		{
			Property(p => p.Name).IsRequired().HasMaxLength(200);
		}
	}
}
