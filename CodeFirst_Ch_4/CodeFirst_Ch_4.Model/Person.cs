﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeFirst_Ch_4.Model
{
	public class Person
	{
		public Person()
		{
			this.Address = new Address();

			// note that PersonalInfo contains the Measurement ComplexType. This is how we make them columns in the DB
			this.Info = new PersonalInfo()
			{
				Weight = new Measurement(),
				Height = new Measurement()
			};
		}

		public int PersonId { get; set; }
		public long SocialSecurityNumber { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public Address Address { get; set; }
		public PersonalInfo Info { get; set; }

		// page 3093
		// Inverse Navigation Properties
		public List<Lodging> PrimaryContactFor { get; set; }
		public List<Lodging> SecondaryContactFor { get; set; }

		public byte[] RowVersion { get; set; }
	}
}
