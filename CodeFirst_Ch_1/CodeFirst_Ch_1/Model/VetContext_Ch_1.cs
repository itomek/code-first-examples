﻿using System.Data.Entity;

namespace CodeFirst_Ch_1.Model
{
	public class VetContext_Ch_1 : DbContext
	{
		public DbSet<Patient> Patients { get; set; }
		public DbSet<Visit> Visits { get; set; }

		public int SavePatient(Patient patient)
		{
			Patient newPatient = this.Patients.Add(patient);
			this.SaveChanges();

			return newPatient.Id;
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<AnimalType>().ToTable("Species");
			modelBuilder.Entity<AnimalType>().Property(p => p.TypeName).IsRequired(); // makes the column NOT NULL
		}
	}
}
