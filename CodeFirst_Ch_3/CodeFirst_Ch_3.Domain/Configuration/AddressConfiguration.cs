﻿using CodeFirst_Ch_3.Model;
using System.Data.Entity.ModelConfiguration;

namespace CodeFirst_Ch_3.Domain.Configuration
{
	public class AddressConfiguration : ComplexTypeConfiguration<Address>
	{
		public AddressConfiguration()
		{
			this.Property(p => p.StreetAddress).HasMaxLength(150);
		}
	}
}
