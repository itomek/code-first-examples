﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace CodeFirst_Ch_2.Model
{
	public class Lodging
	{
		public int LodgingId { get; set; }

		[Required]
		[MaxLength(200)]
		[MinLength(10)]
		public string Name { get; set; }
		public string Owner { get; set; }
		public bool IsResort { get; set; }

		public Destination Destination { get; set; }
	}
}
