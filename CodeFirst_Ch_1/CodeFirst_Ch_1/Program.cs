﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeFirst_Ch_1.Model;

namespace CodeFirst_Ch_1
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine(); Console.Write("Type of Animal: ");

			var dog = new AnimalType { TypeName = Console.In.ReadLine() };

			Console.WriteLine(); Console.Write("Name of Patient: ");

			var patient = new Patient()
			{
				Name = Console.In.ReadLine(),
				BirthDate = new DateTime(2008, 1, 28),
				AnimalType = dog,
				Visits = new List<Visit>()
				{
					new Visit() { Date = new DateTime(2011, 9, 1) }
				}
			};

			int result = default(int);

			using (var contxt = new VetContext_Ch_1())
			{
				result = contxt.SavePatient(patient);
			}

			Console.WriteLine("Patient saved with ID: {0}", result);

			using (var contxt = new VetContext_Ch_1())
			{
				var dogs = contxt.Patients.Where(x => x.AnimalType.TypeName == "Dog").ToList();
			}
		}
	}
}
