﻿using System.Data.Entity;
using CodeFirst_Ch_2.Model;
using System.Collections.Generic;
using CodeFirst_Ch_2.Domain.Configuration;

namespace CodeFirst_Ch_2.Domain
{
	public class BreakAwayContext_Ch_2 : DbContext
	{
		public DbSet<Destination> Destination { get; set; }
		public DbSet<Lodging> Lodging { get; set; }

		public int SaveDestination(Destination destination)
		{
			Destination newDestination = this.Destination.Add(destination);
			this.SaveChanges();

			return newDestination.DestinationId;
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new DestinationConfiguration());

			modelBuilder.Configurations.Add(new LodgingConfigurationcs());

			/*
			// the above code moves the below commented out code to its own confi classes
			modelBuilder.Entity<Destination>().Property(p => p.Name).IsRequired();
			modelBuilder.Entity<Destination>().Property(p => p.Description).HasMaxLength(500);
			modelBuilder.Entity<Destination>().Property(p => p.Photo).HasColumnType("image");
			modelBuilder.Entity<Lodging>().Property(p => p.Name).IsRequired().HasMaxLength(200);
			*/
		}
	}
}
