﻿using System.Data.Entity.ModelConfiguration;
using CodeFirst_Ch_3.Model;

namespace CodeFirst_Ch_3.Domain.Configuration
{
	public class DestinationConfiguration : EntityTypeConfiguration<Destination>
	{
		public DestinationConfiguration()
		{
			this.Property(p => p.Name).IsRequired();

			this.Property(p => p.Description).HasMaxLength(500);

			this.Property(p => p.Photo).HasColumnType("image");

			// Optional One-to-Many relationship
			// this is redundant as Code First makes the same decision from the
			// convention of linking these two classes.
			// This would be useful if we didn't specify a Foreign Key by convention
			// but rather did something like: public Destination Destination { get; set; }
			//this.HasMany(d => d.Lodgings).WithOptional(l => l.Destination);
		}
	}
}
