﻿using CodeFirst_Ch_3.Model;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations;

namespace CodeFirst_Ch_3.Domain.Configuration
{
	public class TripConfiguration : EntityTypeConfiguration<Trip>
	{
		public TripConfiguration()
		{
			this.HasKey(k => k.Identifier);

			// because we are using a GUID as our identifier, we must specify, otherwise the guid won't be
			// auto generated
			this.Property(p => p.Identifier).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

			this.Property(p => p.StartDate).HasColumnType("datetime2");

			this.Property(p => p.EndDate).HasColumnType("datetime2");

			// setting field for concurrancy
			this.Property(p => p.RowVersion).IsRowVersion();
		}
	}
}
