﻿using System.Data.Entity.ModelConfiguration;
using CodeFirst_Ch_2.Model;

namespace CodeFirst_Ch_2.Domain.Configuration
{
	public class DestinationConfiguration : EntityTypeConfiguration<Destination>
	{
		public DestinationConfiguration()
		{
			Property(p => p.Name).IsRequired();

			Property(p => p.Description).HasMaxLength(500);

			Property(p => p.Photo).HasColumnType("image");
		}
	}
}
