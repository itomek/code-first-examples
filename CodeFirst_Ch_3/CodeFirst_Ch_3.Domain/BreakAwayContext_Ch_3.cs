﻿using System.Data.Entity;
using CodeFirst_Ch_3.Model;
using System.Collections.Generic;
using CodeFirst_Ch_3.Domain.Configuration;
using System;

namespace CodeFirst_Ch_3.Domain
{
	public class BreakAwayContext_Ch_3 : DbContext
	{
		public DbSet<Destination> Destination { get; set; }
		public DbSet<Lodging> Lodging { get; set; }
		public DbSet<Trip> Trips { get; set; }
		public DbSet<Person> People { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new DestinationConfiguration());

			modelBuilder.Configurations.Add(new LodgingConfigurationcs());

			modelBuilder.Configurations.Add(new TripConfiguration());

			modelBuilder.Configurations.Add(new PersonConfiguration());

			modelBuilder.Configurations.Add(new AddressConfiguration());

			modelBuilder.Configurations.Add(new PersonalInfoConfiguration());
		}

		public int SaveDestination(Destination destination)
		{
			Destination newDestination = this.Destination.Add(destination);
			this.SaveChanges();

			return newDestination.DestinationId;
		}

		public Guid SaveTrip(Trip trip)
		{
			Trip newTrip = this.Trips.Add(trip);
			this.SaveChanges();

			return newTrip.Identifier;
		}

		public Guid UpdateTrip(Guid id, decimal cost)
		{
			var trip = this.Trips.Find(id);

			trip.CostUSD = cost;
			this.SaveChanges();

			return trip.Identifier;
		}

		public long SavePerson(Person person)
		{
			Person newPerson = this.People.Add(person);
			this.SaveChanges();

			return newPerson.SocialSecurityNumber;
		}
	}
}
