﻿using System.Data.Entity.ModelConfiguration;
using CodeFirst_Ch_4.Model;

namespace CodeFirst_Ch_4.Domain.Configuration
{
	public class LodgingConfigurationcs : EntityTypeConfiguration<Lodging>
	{
		public LodgingConfigurationcs()
		{
			this.Property(p => p.Name).IsRequired().HasMaxLength(200);

			// we don't want Owner to be a Unicode field
			// this changes the field from NVARCHAR to VARCHAR
			this.Property(p => p.Owner).IsUnicode(false);

			// add precision to field
			this.Property(p => p.MilesFromNearestAirport).HasPrecision(8, 1);

			// page 3093
			// Inverse Navigation Properties
			this.HasOptional(l => l.PrimaryContact).WithMany(p => p.PrimaryContactFor);
			this.HasOptional(l => l.SecondaryContact).WithMany(p => p.SecondaryContactFor);
		}
	}
}
